import json
from collections import defaultdict
from datetime import datetime
from LambdaPage import LambdaPage
from pydq import TIME_FORMAT
import logging
import os
from io import BytesIO
import pandas as pd


logger = logging.getLogger('GDBAPI')


def get(event):
    dq_provider = event['context']['dq_provider']
    params = defaultdict(lambda: None)
    params.update(event['queryStringParameters'])
    bname = params['bname']
    qids = params['qids']
    if qids is None:
        qids = [None]
    else:
        qids = qids.split(',')
    start_time = params['start_time']
    start_time = datetime.strptime(start_time, TIME_FORMAT) if start_time is not None else datetime(1, 1, 1)
    end_time = params['end_time']
    end_time = datetime.strptime(end_time, TIME_FORMAT) if end_time is not None else datetime.utcnow()
    limit = int(params['limit']) if params['limit'] is not None else 0
    logger.info('Accessing %s from %s to %s limit %i' % (bname, start_time, end_time, limit))
    with dq_provider(bname) as dq:
        dataframes = []
        for qid in qids:
            dq(qid, start_time, end_time, limit)
            df = dq.get_to_dataframe()
            if len(df) == 0:
                continue
            if 'sample_rate' in params:
                sample_rate = params['sample_rate']
                sample_method = params['sample_method'] if 'sample_method' in params else 'mean'
                offset = params['sample_offset'] if 'sample_offset' in params else None
                df = df.groupby(['qid']).resample(sample_rate, on='ts', loffset=offset).aggregate(sample_method)
                df = df.dropna(subset=['val'])
                if 'interpolator' in params:
                    # FIXME: can't use anything but linear
                    interpolator = params['interpolator']
                    df = df.interpolate(interpolator)
            dataframes.append(df)

        if len(dataframes) == 0:
            return 200, []
    
        if 'agg' in params:
            result_df = dataframes.pop(0)
            for df in dataframes:
                if len(df) == 0:
                    continue
                result_df = result_df.merge(df, on='ts')
                result_df['val'] = result_df[['val_x', 'val_y']].aggregate(params['agg'], axis=1)
                del (result_df['val_x'])
                del (result_df['val_y'])
            if len(result_df) > 0:
                result_df['qid'] = params['agg'] + '(' + ','.join(qids) + ')'
                if isinstance(result_df.index, pd.core.index.MultiIndex):
                    result_df.index = result_df.index.rename(['iqid', 'ts'])
        else:
            result_df = pd.concat(dataframes)

    result = [{'qid': d['qid'], 'ts': pd.to_datetime(d['ts']).strftime(TIME_FORMAT), 'val': d['val']}
              for d in result_df.to_records()]
    return 200, result


def _validate(item):
    return isinstance(item, dict) and all(e in item for e in ['qid', 'ts', 'val']) and isinstance(item['qid'], str) \
           and isinstance(item['ts'], str)


def put(event):
    dq_provider = event['context']['dq_provider']
    params = defaultdict(lambda: None)
    params.update(event['queryStringParameters'])
    bname = params['bname']
    validate = params['validate'].lower() != 'false' if params['validate'] is not None else True
    items = json.loads(event['body'])
    logger.info('Receiving %s with %i item(s).' % (bname, len(items)))
    if validate:
        for idx, item in enumerate(items):
            if not _validate(item):
                return 400, 'Failed validation at index %i: %s' % (idx, json.dumps(item))
    with dq_provider(bname) as dq:
        dq.put_all(items)
        return 202, 'Accepted'


def get_sites(event):
    dq_provider = event['context']['dq_provider']
    with dq_provider('__system__') as dq:
        sites = json.loads(dq('sites').get()['val'])
    if event['queryStringParameters'] is not None and 'includeDashboards' in event['queryStringParameters'] and \
            event['queryStringParameters']['includeDashboards'] == 'true':
        for name, site in sites.items():
            try:
                site.update(json.loads(event['context']['_get_resource']('client/sites/%s.json' % name, event)))
            except:
                site['dashboards'] = []
                site['streams'] = []
    return 200, sites


def get_resource(event):
    res = event['pathParameters']['resource']
    extension = res.split('.')[-1]
    if extension == 'js':
        get_resource.content_type = 'text/js'
    elif extension == 'css':
        get_resource.content_type = 'text/css'
    elif extension == 'json':
        get_resource.content_type = 'application/json'
    else:
        get_resource.content_type = 'text/html'
    return 200, event['context']['_get_resource']('client/%s' % res, event)


def _get_resource_s3(name, event):
    import boto3
    b = BytesIO()
    print('getting resource from s3: ' + name)
    boto3.resource('s3').Bucket(os.environ['BUCKET_NAME']).download_fileobj(name, b)
    b.seek(0)
    return b.read().decode('utf-8')


def _get_resource_local(name, event):
    with open(name) as f:
        return f.read()


def init_lambda_page(dq_provider):
    page = LambdaPage()
    page.context['dq_provider'] = dq_provider
    page.add_endpoint('get', '/sites', get_sites, 'application/json')
    page.add_endpoint('get', '/client/{resource}', get_resource, 'text/html')
    page.add_endpoint('get', '/', get, 'application/json')
    page.add_endpoint('put', '/', put, 'application/json')
    return page


def lambda_handler(event, context):
    from pydq.cloud import DynamoDB
    logger.info(json.dumps(event))
    page = init_lambda_page(DynamoDB)
    page.context['_get_resource'] = _get_resource_s3
    return page.handle_request(event)


if __name__ == '__main__':
    from pydq.cloud import DynamoDB
    import boto3
    boto3.setup_default_session(profile_name='grieg')
    page = init_lambda_page(DynamoDB)
    page.context['_get_resource'] = _get_resource_s3
    page.start_local()
