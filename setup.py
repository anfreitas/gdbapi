from setuptools import setup, find_packages

with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name='gdbapi',
    version='0.0.8',
    author='Andrew Freitas',
    author_email='andrewfreitas09@gmail.com',
    description='Simple get/put API for GDB applications. Targeted at HTTP(S)',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/anfreitas/gdbapi',
    packages=["."],
    install_requires=["LambdaPage", "pydq"],
    extras_require={"aws": ["boto3"], "local": ["falcon"]},
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ),
)
